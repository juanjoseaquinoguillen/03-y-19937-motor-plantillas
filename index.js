const express = require('express');
const hbs = require('hbs');
const app = express();
require('dotenv').config({ path: '.env' });
//const db = require( './db/data' );anterior llamado a la bd simulada
//importar archivo de rutas
const router= require ('./routes/public')
app.use("/", router)

//creacion de aplicacion express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en el puerto ${puerto}`);
});
